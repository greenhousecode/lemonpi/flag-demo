(ns flag-demo.core
  (:require [flappie.core :as flap]
            [reagent.core :as r]
            [reagent.dom :as rdom]))

(goog-define UNLEASH-URL "")
(goog-define UNLEASH-INSTANCE-ID "")


(defonce acceptance-client
  (flap/client {:adapter             :unleash
                :refresh-interval-ms 10e3
                :atom-fn             reagent.core/atom
                :unleash/url         UNLEASH-URL
                :unleash/app-name    "acceptance"
                :unleash/instance-id UNLEASH-INSTANCE-ID}))


(defonce production-client
  (flap/client {:adapter             :unleash
                :refresh-interval-ms 10e3
                :atom-fn             reagent.core/atom
                :unleash/url         UNLEASH-URL
                :unleash/app-name    "production"
                :unleash/instance-id UNLEASH-INSTANCE-ID}))


(defonce local-client (flap/client {:adapter     :local
                                    :local/flags {:some.component/feature true}}))


(defn flags-component [{:keys [client]}]
  [:dl
   (doall
    (for [[flag-name flag] @(:flags client)]
      ^{:key flag-name}
      [:<>
       [:dt (str flag-name)]
       [:dd (str (:description flag "<no description>") " - " (flap/enabled? client flag-name))]]))])


(defn root-view []
  [:<>
   (doall (for [[client-name client] {"Acceptance" acceptance-client
                                      "Production" production-client
                                      "Local"      local-client}]
            ^{:key client-name}
            [:<>
             [:h2 client-name]
             [flags-component {:client client}]]))])


(defn ^:dev/after-load render
  []
  (rdom/render [root-view]
               (js/document.getElementById "app")))


(defn main! []
  (render))


(comment

  )
